/*
	dic := morpherus.NewDic()
	defer dic.Close()

	dic.TryNounInForm("Компьютер", RoditelniyPad, MnozhCh)
	// Компьютеров

*/
package morpherus

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"encoding/json"

	"log"

	"time"

	"github.com/boltdb/bolt"
)

//var defaultDicPath = os.Getenv("GOPATH") + "/src/bitbucket.org/akhalyapin/morpherus/dic.gob"
var (
	ErrNotExists = errors.New("Doesn't exists")

	defaultDicPath = os.Getenv("GOPATH") + "/src/bitbucket.org/akhalyapin/morpherus/dic.db"
	dicName        = []byte("Dic")
)

type WordType int

const (
	UnknownType WordType = iota
	Glagol
	ZhenskyRod
	MuzhskoyRod
	Mnozhestvennoe
	SredniyRod
	Prilagatelnoe
	Special
	Familiya
	Chislitelnoe
)

var wordTypeStr = [...]string{
	"--",
	"Глагол",
	"Женский род",
	"Мужской род",
	"Множественное",
	"Средний род",
	"Прилагательное",
	"Специальное",
	"Фамилии",
	"Числительные",
}

func (a WordType) String() string { return wordTypeStr[a] }
func (a WordType) IsNoun() bool {
	switch a {
	case ZhenskyRod, MuzhskoyRod, Mnozhestvennoe, SredniyRod, Chislitelnoe:
		return true
	}
	return false
}

type WordForm int

const (
	UnknownPad WordForm = iota
	ImenitelniyPad
	RoditelniyPad
	DatelniyPad
	VinitelniyPad
	TvoritelniyPad
	PredlozhniyPad
)

var padezhStr = [...]string{
	"--",
	"Именительный",
	"Родительный",
	"Дательный",
	"Винительный",
	"Творительный",
	"Предложный",
}

func (a WordForm) String() string { return padezhStr[a] }

type Chislo int

const (
	EdinCh Chislo = iota
	MnozhCh
)

var chisloStr = [...]string{
	"Единственное число",
	"Множественное число",
}

func (a Chislo) String() string { return chisloStr[a] }

// Map for padezh - suffix id
// int =  index of suffix
var mapNounFormsForType = map[WordType]map[Chislo]map[WordForm]int{
	MuzhskoyRod: map[Chislo]map[WordForm]int{
		EdinCh: map[WordForm]int{
			ImenitelniyPad: 0,
			RoditelniyPad:  1,
			DatelniyPad:    2,
			VinitelniyPad:  0,
			TvoritelniyPad: 3,
			PredlozhniyPad: 4,
		},
		MnozhCh: map[WordForm]int{
			ImenitelniyPad: 5,
			RoditelniyPad:  6,
			DatelniyPad:    7,
			VinitelniyPad:  5,
			TvoritelniyPad: 8,
			PredlozhniyPad: 9,
		},
	},
	ZhenskyRod: map[Chislo]map[WordForm]int{
		EdinCh: map[WordForm]int{
			ImenitelniyPad: 0,
			RoditelniyPad:  1,
			DatelniyPad:    2,
			VinitelniyPad:  3,
			TvoritelniyPad: 4,
			PredlozhniyPad: 6,
		},
		MnozhCh: map[WordForm]int{
			ImenitelniyPad: 7,
			RoditelniyPad:  8,
			DatelniyPad:    9,
			VinitelniyPad:  7,
			TvoritelniyPad: 10,
			PredlozhniyPad: 11,
		},
	},
	Mnozhestvennoe: map[Chislo]map[WordForm]int{
		EdinCh: map[WordForm]int{
			ImenitelniyPad: 0,
			RoditelniyPad:  1,
			DatelniyPad:    2,
			VinitelniyPad:  0,
			TvoritelniyPad: 3,
			PredlozhniyPad: 4,
		},
		MnozhCh: map[WordForm]int{
			ImenitelniyPad: 0,
			RoditelniyPad:  1,
			DatelniyPad:    2,
			VinitelniyPad:  0,
			TvoritelniyPad: 3,
			PredlozhniyPad: 4,
		},
	},
	SredniyRod: map[Chislo]map[WordForm]int{
		EdinCh: map[WordForm]int{
			ImenitelniyPad: 0,
			RoditelniyPad:  1,
			DatelniyPad:    2,
			VinitelniyPad:  0,
			TvoritelniyPad: 3,
			PredlozhniyPad: 4,
		},
		MnozhCh: map[WordForm]int{
			ImenitelniyPad: 5,
			RoditelniyPad:  6,
			DatelniyPad:    7,
			VinitelniyPad:  5,
			TvoritelniyPad: 8,
			PredlozhniyPad: 9,
		},
	},
	// Not accurate due to lack of suffix variants in some words
	Chislitelnoe: map[Chislo]map[WordForm]int{
		EdinCh: map[WordForm]int{
			ImenitelniyPad: 0,
			RoditelniyPad:  0,
			DatelniyPad:    1,
			VinitelniyPad:  0,
			TvoritelniyPad: 2,
			PredlozhniyPad: 1, // can be 3, but not for all
		},
		MnozhCh: map[WordForm]int{
			ImenitelniyPad: 0,
			RoditelniyPad:  0,
			DatelniyPad:    1,
			VinitelniyPad:  0,
			TvoritelniyPad: 2,
			PredlozhniyPad: 1,
		},
	},
}

var mapAdjectiveFormsForType = map[WordType]map[Chislo]map[WordForm]int{
	MuzhskoyRod: map[Chislo]map[WordForm]int{
		EdinCh: map[WordForm]int{
			ImenitelniyPad: 15,
			RoditelniyPad:  8,
			DatelniyPad:    12,
			VinitelniyPad:  15,
			TvoritelniyPad: 16,
			PredlozhniyPad: 11,
		},
		MnozhCh: map[WordForm]int{
			ImenitelniyPad: 14,
			RoditelniyPad:  18,
			DatelniyPad:    16,
			VinitelniyPad:  14,
			TvoritelniyPad: 17,
			PredlozhniyPad: 18,
		},
	},
	ZhenskyRod: map[Chislo]map[WordForm]int{
		EdinCh: map[WordForm]int{
			ImenitelniyPad: 7,
			RoditelniyPad:  10,
			DatelniyPad:    10,
			VinitelniyPad:  13,
			TvoritelniyPad: 10,
			PredlozhniyPad: 10,
		},
		MnozhCh: map[WordForm]int{
			ImenitelniyPad: 14,
			RoditelniyPad:  18,
			DatelniyPad:    16,
			VinitelniyPad:  14,
			TvoritelniyPad: 17,
			PredlozhniyPad: 18,
		},
	},
	SredniyRod: map[Chislo]map[WordForm]int{
		EdinCh: map[WordForm]int{
			ImenitelniyPad: 9,
			RoditelniyPad:  8,
			DatelniyPad:    12,
			VinitelniyPad:  9,
			TvoritelniyPad: 16,
			PredlozhniyPad: 11,
		},
		MnozhCh: map[WordForm]int{
			ImenitelniyPad: 14,
			RoditelniyPad:  18,
			DatelniyPad:    16,
			VinitelniyPad:  14,
			TvoritelniyPad: 17,
			PredlozhniyPad: 18,
		},
	},
	Mnozhestvennoe: map[Chislo]map[WordForm]int{
		EdinCh: map[WordForm]int{
			ImenitelniyPad: 14,
			RoditelniyPad:  18,
			DatelniyPad:    16,
			VinitelniyPad:  14,
			TvoritelniyPad: 17,
			PredlozhniyPad: 18,
		},
		MnozhCh: map[WordForm]int{
			ImenitelniyPad: 14,
			RoditelniyPad:  18,
			DatelniyPad:    16,
			VinitelniyPad:  14,
			TvoritelniyPad: 17,
			PredlozhniyPad: 18,
		},
	},
}

func NewDic() *Dic {
	return &Dic{}
}

// Word Variant
type WVar struct {
	Root string
	WTyp WordType
	Suf  []string
}

// Full name with suffix
func (w WVar) fullWord(suffixId int) string {
	if len(w.Suf) > suffixId {
		switch w.Suf[suffixId] {
		case "?":
			return ""
		case "#":
			return w.Root
		default:
			return w.Root + w.Suf[suffixId]
		}
	}
	return ""
}

func (w *WVar) String() string {
	sufStr := "[ "
	for i, suf := range w.Suf {
		sufStr += fmt.Sprintf("%d:%s, ", i, suf)
	}
	sufStr += " ]"
	return fmt.Sprintf("%s, %s, %s",
		w.Root, w.WTyp, sufStr)
}

type Dic struct {
	db *bolt.DB
}

// if path is empty - open default db path
func (d *Dic) Open(path ...string) {
	d.open(true, time.Second, path...)
}

func (d *Dic) OpenForWrite(path ...string) {
	d.open(false, time.Second*3, path...)
}

func (d *Dic) open(readOnly bool, timeout time.Duration, path ...string) {
	if d.db != nil {
		return
	}
	dbPath := defaultDicPath
	if len(path) > 0 {
		dbPath = path[0]
	}
	opt := &bolt.Options{ReadOnly: readOnly, Timeout: timeout}
	db, err := bolt.Open(dbPath, 0600, opt)
	if err != nil {
		log.Printf("DB didn't open. Err: %s", err)
	} else {
		d.db = db
	}
	db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte(dicName))
		if err != nil {
			log.Printf("Create bucket: %s", err)
		}
		return nil
	})
}

// Close dictionary db
func (d Dic) Close() {
	d.db.Close()
}

// If word doesn't exists -
func (d Dic) GetWordVariants(word string) ([]WVar, error) {
	wvs := []WVar{}
	err := d.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(dicName)
		v := b.Get([]byte(word))
		if v == nil {
			return ErrNotExists
		}
		var wv WVar
		err := json.Unmarshal(v, &wv)
		if err != nil {
			return err
		}
		wvs = append(wvs, wv)
		return nil
	})
	if err != nil {
		return []WVar{}, err
	}
	return wvs, nil
}

func (d Dic) SaveWordVariant(word string, variant WVar) error {
	data, err := json.Marshal(variant)
	if err != nil {
		return err
	}
	d.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(dicName)
		err := b.Put([]byte(word), data)
		return err
	})
	return nil
}

func (d *Dic) AddWord(root string, wordType WordType, suf []string) error {
	root = strings.ToLower(root)
	wordVariant := WVar{
		Root: root,
		WTyp: wordType,
		Suf:  suf,
	}

	for _, sf := range suf {
		if sf == "#" {
			sf = ""
		}
		if sf == "?" {
			continue
		}
		word := root + sf

		hasWordType := false

		variants, err := d.GetWordVariants(word)

		if err != nil && err != ErrNotExists {
			return errors.New(fmt.Sprintf("Can't get the word: %s. Err: %s", word, err))
		}

		for _, wv := range variants {
			if wv.WTyp == wordType {
				hasWordType = true
			}
		}

		if !hasWordType {
			err := d.SaveWordVariant(word, wordVariant)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

// TryGetWordInForm equal to WordInForm, but no return error. Returns original word in case of error.
func (d Dic) TryNounInForm(word string, form WordForm, chis Chislo) string {
	s, _ := d.NounInForm(word, form, chis)
	return s
}

func (d Dic) TryNounInTypedForm(word string, wordType WordType, form WordForm, chis Chislo) string {
	s, _ := d.NounInTypedForm(word, wordType, form, chis)
	return s
}

// WordInForm gets word in specific padezh and chislo
func (d Dic) NounInForm(word string, form WordForm, chis Chislo) (string, error) {
	return d.NounInTypedForm(word, UnknownType, form, chis)
}

// WordType can be Unknown, in that case get first matched type
func (d Dic) NounInTypedForm(word string, wordType WordType, form WordForm, chis Chislo) (string, error) {
	s, _, err := d.wordInTypedForm(word, true, wordType, form, chis)
	return s, err
}

func (d Dic) TryAdjInForm(word string, form WordForm, chis Chislo) string {
	s, _ := d.AdjectiveInTypedForm(word, UnknownType, form, chis)
	return s
}

func (d Dic) TryAdjectiveInTypedForm(word string, compatibleWordType WordType, form WordForm, chis Chislo) string {
	s, _ := d.AdjectiveInTypedForm(word, compatibleWordType, form, chis)
	return s
}

// compatibleWordType - type of compatible word (noun)
func (d Dic) AdjectiveInTypedForm(word string, compatibleWordType WordType, form WordForm, chis Chislo) (string, error) {
	// Default type
	if compatibleWordType == UnknownType {
		compatibleWordType = MuzhskoyRod
	}
	s, _, err := d.wordInTypedForm(word, false, compatibleWordType, form, chis)
	return s, err
}

// If multiple word types will found, will use prefferedType, if it's Unknown - first found type
func (d Dic) wordInTypedForm(word string, isNoun bool, prefferedType WordType, form WordForm, chis Chislo) (string, WordType, error) {
	wordLow := strings.ToLower(word)

	variants, err := d.GetWordVariants(wordLow)
	if err != nil {
		return word, UnknownType, errors.New("Word not found")
	}

	for _, variant := range variants {
		var mapForSearch map[WordType]map[Chislo]map[WordForm]int

		if isNoun && variant.WTyp == prefferedType || (prefferedType == UnknownType && variant.WTyp.IsNoun()) {
			prefferedType = variant.WTyp
			mapForSearch = mapNounFormsForType
		} else if !isNoun && variant.WTyp == Prilagatelnoe {
			// for adj
			mapForSearch = mapAdjectiveFormsForType
		} else {
			return word, UnknownType, errors.New("Word params not found. May be word isn't Noun")
		}

		if sufIdx, ok := mapForSearch[prefferedType][chis][form]; ok {

			res := variant.fullWord(sufIdx)
			if res == "" {
				return word, UnknownType, errors.New("Unknown suffix")
			}
			return res, variant.WTyp, nil

		}
	}
	return word, prefferedType, errors.New("Word type not found")
}

func (d Dic) TryWordsToSameForm(noun, nounOrAdj string, form WordForm, chis Chislo) (string, string) {
	s1, s2, _ := d.WordsToSameForm(noun, nounOrAdj, true, form, chis)
	return s1, s2
}

// Two compatible words
func (d Dic) WordsToSameForm(noun, nounOrAdj string, ifNotAdjFirstForm bool, form WordForm, chis Chislo) (string, string, error) {
	wordNounLow := strings.ToLower(noun)
	wordNounOrAdjLow := strings.ToLower(nounOrAdj)

	s1, wordType, err := d.wordInTypedForm(wordNounLow, true, UnknownType, form, chis)
	if err != nil {
		return noun, nounOrAdj, err
	}
	// if second adj ?
	isAdj, _ := d.IsAdjective(nounOrAdj)

	s2 := nounOrAdj

	if isAdj {
		s2, _, err = d.wordInTypedForm(wordNounOrAdjLow, false, wordType, form, chis)
		if err != nil {
			return s1, s2, err
		}
	} else {
		// If second not adjective - word in firs form
		if ifNotAdjFirstForm {
			form = ImenitelniyPad
			chis = EdinCh
		}
		s2, _, err = d.wordInTypedForm(wordNounOrAdjLow, true, UnknownType, form, chis)
		if err != nil {
			return s1, s2, err
		}
	}
	return s1, s2, nil
}

func (d Dic) WordType(word string) (WordType, error) {
	vars := d.WordVariants(word)
	for _, wv := range vars {
		if wv.WTyp.IsNoun() {
			return wv.WTyp, nil
		}
	}
	return UnknownType, errors.New("Type not found")
}

func (d Dic) TryIsAdjective(word string) bool {
	isIt, _ := d.IsAdjective(word)
	return isIt
}

func (d Dic) IsAdjective(word string) (bool, error) {
	wordLow := strings.ToLower(word)
	variants, err := d.GetWordVariants(wordLow)
	if err != nil {
		return false, err
	}
	for _, variant := range variants {
		if variant.WTyp == Prilagatelnoe {
			return true, nil
		}
	}
	return false, errors.New("Word not found")
}

func (d Dic) TryIsNounInFirstForm(word string) bool {
	isIt, _ := d.IsNounInFirstForm(word)
	return isIt
}

func (d Dic) IsNounInFirstForm(word string) (bool, error) {
	vars := d.WordVariants(word)
	for _, wv := range vars {
		if wv.WTyp.IsNoun() {
			if wv.fullWord(0) == word {
				return true, nil
			}
			return false, nil
		}
	}
	return false, errors.New("Type not found")
}

//
func (d Dic) IsNoun(word string) (bool, error) {
	vars := d.WordVariants(word)
	if len(vars) == 0 {
		return false, errors.New("Word not found")
	}
	for _, wv := range vars {
		if wv.WTyp.IsNoun() {
			return true, nil
		}
	}
	return false, nil
}

// Checks Nouns and Adjectives is it plural.
// Also return WordForm (padezh)
// Notice: if it's Adjective or SredniyRod, WordForm may vary.
func (d Dic) WordProp(word string) (isPlural bool, padezh WordForm, wordType WordType, isAdj bool, err error) {
	vars := d.WordVariants(word)
	if len(vars) == 0 {
		err = errors.New("Word not found")
		return
	}
	wordLow := strings.ToLower(word)
	for _, wv := range vars {
		wordType = wv.WTyp
		var mapForSearch map[WordType]map[Chislo]map[WordForm]int
		if wv.WTyp.IsNoun() {
			mapForSearch = mapNounFormsForType
		} else if wv.WTyp == Prilagatelnoe {
			isAdj = true
			mapForSearch = mapAdjectiveFormsForType
		} else {
			err = errors.New("Type not found")
			return
		}

		for wordType, mapWordTypeChisloWordForm := range mapForSearch {
			if (wv.WTyp.IsNoun() && wordType == wv.WTyp) || wv.WTyp == Prilagatelnoe {
				for chislo, WordFormIdx := range mapWordTypeChisloWordForm {
					for wordForm, idx := range WordFormIdx {
						if len(wv.Suf)-1 >= idx && wv.Root+wv.Suf[idx] == wordLow {
							if chislo == MnozhCh {
								isPlural = true
							}
							padezh = wordForm
						}
					}
				}
			}
		}
	}
	return
}

func (d Dic) WordVariants(word string) []WVar {
	word = strings.ToLower(word)
	wordVar, _ := d.GetWordVariants(word)
	return wordVar
}
