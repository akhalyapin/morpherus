package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"

	"bitbucket.org/akhalyapin/morpherus"
	"github.com/djimenez/iconv-go"
	"github.com/gobuild/log"
)

const path string = "data/LexGroup"

var typeMap = map[string]morpherus.WordType{
	"ГЛАГ":   morpherus.Glagol,
	"ЖЕН":    morpherus.ZhenskyRod,
	"МУЖ":    morpherus.MuzhskoyRod,
	"МЧ":     morpherus.Mnozhestvennoe,
	"ОНО":    morpherus.SredniyRod,
	"ПРИЛ":   morpherus.Prilagatelnoe,
	"СПЕЦ":   morpherus.Special,
	"ФАМИЛ":  morpherus.Familiya,
	"ЧИСЛИТ": morpherus.Chislitelnoe,
}

func Import(path string) {

	dic := morpherus.NewDic()
	dic.OpenForWrite()
	defer dic.Close()

	err := WalkDirs(func(word string, wordType morpherus.WordType, suf []string) {
		err := dic.AddWord(word, wordType, suf)
		if err != nil {
			log.Errorf("Can't add the word: %s. Err: %s", word, err)
		}
		log.Printf("Add word: %s", word)
	})
	if err != nil {
		panic(err)
	}
}

func WalkDirs(outFn func(word string, wordType morpherus.WordType, suf []string)) error {

	dirs, err := ioutil.ReadDir(path)

	for _, dir := range dirs {
		if err != nil {
			return err
		}

		if dir.IsDir() {

			files, err := ioutil.ReadDir(fmt.Sprintf("%s%s%s", path, string(os.PathSeparator), dir.Name()))
			if err != nil {
				return err
			}

			for _, file := range files {
				i := 0
				curSuf := []string{}
				err = readLine(fmt.Sprintf("%s%s%s%s%s", path, string(os.PathSeparator), dir.Name(), string(os.PathSeparator), file.Name()),
					func(s string) {
						if i == 0 {
							curSuf = ParseHead(s)
						} else {
							outFn(s, typeMap[dir.Name()], curSuf)
						}
						i++
					})
				if err != nil {
					return err
				}
			}
		}
	}

	return nil

}

func readLine(path string, fn func(out string)) error {
	inFile, err := os.Open(path)
	defer inFile.Close()
	if err != nil {
		return err
	}
	scanner := bufio.NewScanner(inFile)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		output, err := iconv.ConvertString(scanner.Text(), "windows-1251", "utf-8")
		if err != nil {
			return err
		}
		fn(output)
	}
	return nil
}
