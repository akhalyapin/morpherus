package main

import (
	"fmt"
	"os"
)

func main() {

	if len(os.Args) == 1 {
		fmt.Println("No arguments was set.")
		fmt.Println("Usage: importer filename")
	}

	if len(os.Args) == 2 {
		Import(os.Args[1])
	}

}
