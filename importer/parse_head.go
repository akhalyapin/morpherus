package main

func ParseHead(s string) []string {
	sRunes := []rune(s)
	words := []string{}
	root := ""
	curWord := ""
	open := false

	for i, r := range sRunes {
		if i == 0 {
			continue
		}
		if r == ' ' {
			break
		}

		if r == '{' {
			open = true
			curWord = ""
			continue
		}

		if r == '}' {
			open = false
			if root+curWord != "" {
				words = append(words, root+curWord)
			}
			root = ""
			curWord = ""
			continue
		}

		if r == ',' {
			if root+curWord != "" {
				words = append(words, root+curWord)
			}
			curWord = ""
			if !open {
				root = ""
			}
			continue
		}

		if open {
			curWord = curWord + string(r)
		} else {
			root = root + string(r)
		}
	}
	return words
}
