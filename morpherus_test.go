package morpherus

import "testing"

type testCase struct {
	out string

	in    string
	wType WordType
	wForm WordForm
	chisl Chislo
	isAdj bool
}

func TestMorpherus(t *testing.T) {

	dic := NewDic()
	dic.Open()
	defer dic.Close()

	cases := []testCase{
		//		testCase{"хронике", "Хроника", UnknownType, DatelniyPad, EdinCh, false},
		//		testCase{"хроникам", "Хроника", UnknownType, DatelniyPad, MnozhCh, false},

		testCase{"размер", "размер", UnknownType, ImenitelniyPad, EdinCh, false},
		testCase{"размера", "размеры", UnknownType, RoditelniyPad, EdinCh, false},
		testCase{"размеру", "размеров", UnknownType, DatelniyPad, EdinCh, false},
		testCase{"размер", "размерам", UnknownType, VinitelniyPad, EdinCh, false},
		testCase{"размером", "размеру", UnknownType, TvoritelniyPad, EdinCh, false},
		testCase{"размере", "размерами", UnknownType, PredlozhniyPad, EdinCh, false},

		testCase{"размеры", "размер", UnknownType, ImenitelniyPad, MnozhCh, false},
		testCase{"размеров", "размеры", UnknownType, RoditelniyPad, MnozhCh, false},
		testCase{"размерам", "размеров", UnknownType, DatelniyPad, MnozhCh, false},
		testCase{"размеры", "размерам", UnknownType, VinitelniyPad, MnozhCh, false},
		testCase{"размерами", "размеру", UnknownType, TvoritelniyPad, MnozhCh, false},
		testCase{"размерах", "размер", UnknownType, PredlozhniyPad, MnozhCh, false},

		testCase{"ширина", "ширина", UnknownType, ImenitelniyPad, EdinCh, false},
		testCase{"ширины", "ширина", UnknownType, RoditelniyPad, EdinCh, false},
		testCase{"ширине", "ширина", UnknownType, DatelniyPad, EdinCh, false},
		testCase{"ширину", "ширина", UnknownType, VinitelniyPad, EdinCh, false},
		testCase{"шириной", "ширина", UnknownType, TvoritelniyPad, EdinCh, false},
		testCase{"ширине", "ширина", UnknownType, PredlozhniyPad, EdinCh, false},

		testCase{"хлопья", "хлопья", UnknownType, ImenitelniyPad, MnozhCh, false},
		testCase{"хлопьев", "хлопья", UnknownType, RoditelniyPad, MnozhCh, false},
		testCase{"хлопьям", "хлопья", UnknownType, DatelniyPad, MnozhCh, false},
		testCase{"хлопья", "хлопья", UnknownType, VinitelniyPad, MnozhCh, false},
		testCase{"хлопьями", "хлопья", UnknownType, TvoritelniyPad, MnozhCh, false},
		testCase{"хлопьях", "хлопья", UnknownType, PredlozhniyPad, MnozhCh, false},

		testCase{"хлопья", "хлопья", UnknownType, ImenitelniyPad, EdinCh, false},
		testCase{"хлопьев", "хлопья", UnknownType, RoditelniyPad, EdinCh, false},
		testCase{"хлопьям", "хлопья", UnknownType, DatelniyPad, EdinCh, false},
		testCase{"хлопья", "хлопья", UnknownType, VinitelniyPad, EdinCh, false},
		testCase{"хлопьями", "хлопья", UnknownType, TvoritelniyPad, EdinCh, false},
		testCase{"хлопьях", "хлопья", UnknownType, PredlozhniyPad, EdinCh, false},

		testCase{"солнце", "солнцами", UnknownType, ImenitelniyPad, EdinCh, false},
		testCase{"солнца", "солнцами", UnknownType, RoditelniyPad, EdinCh, false},
		testCase{"солнцу", "солнцами", UnknownType, DatelniyPad, EdinCh, false},
		testCase{"солнце", "солнцами", UnknownType, VinitelniyPad, EdinCh, false},
		testCase{"солнцем", "солнцами", UnknownType, TvoritelniyPad, EdinCh, false},
		testCase{"солнце", "солнцами", UnknownType, PredlozhniyPad, EdinCh, false},

		testCase{"пять", "пять", UnknownType, ImenitelniyPad, EdinCh, false},
		testCase{"пять", "пять", UnknownType, RoditelniyPad, EdinCh, false},
		testCase{"пяти", "пять", UnknownType, DatelniyPad, EdinCh, false},
		testCase{"пять", "пять", UnknownType, VinitelniyPad, EdinCh, false},
		testCase{"пятью", "пять", UnknownType, TvoritelniyPad, EdinCh, false},
		testCase{"пяти", "пять", UnknownType, PredlozhniyPad, EdinCh, false},

		// Adj
		testCase{"красный", "красного", MuzhskoyRod, ImenitelniyPad, EdinCh, true},
		testCase{"красного", "красного", MuzhskoyRod, RoditelniyPad, EdinCh, true},
		testCase{"красному", "красного", MuzhskoyRod, DatelniyPad, EdinCh, true},
		testCase{"красный", "красного", MuzhskoyRod, VinitelniyPad, EdinCh, true},
		testCase{"красным", "красного", MuzhskoyRod, TvoritelniyPad, EdinCh, true},
		testCase{"красном", "красного", MuzhskoyRod, PredlozhniyPad, EdinCh, true},

		testCase{"красные", "красного", MuzhskoyRod, ImenitelniyPad, MnozhCh, true},
		testCase{"красных", "красного", MuzhskoyRod, RoditelniyPad, MnozhCh, true},
		testCase{"красным", "красного", MuzhskoyRod, DatelniyPad, MnozhCh, true},
		testCase{"красные", "красного", MuzhskoyRod, VinitelniyPad, MnozhCh, true},
		testCase{"красными", "красного", MuzhskoyRod, TvoritelniyPad, MnozhCh, true},
		testCase{"красных", "красного", MuzhskoyRod, PredlozhniyPad, MnozhCh, true},

		testCase{"красная", "красного", ZhenskyRod, ImenitelniyPad, EdinCh, true},
		testCase{"красной", "красного", ZhenskyRod, RoditelniyPad, EdinCh, true},
		testCase{"красной", "красного", ZhenskyRod, DatelniyPad, EdinCh, true},
		testCase{"красную", "красного", ZhenskyRod, VinitelniyPad, EdinCh, true},
		testCase{"красной", "красного", ZhenskyRod, TvoritelniyPad, EdinCh, true},
		testCase{"красной", "красного", ZhenskyRod, PredlozhniyPad, EdinCh, true},

		// Equal to muzh
		testCase{"красные", "красного", ZhenskyRod, ImenitelniyPad, MnozhCh, true},
		testCase{"красных", "красного", ZhenskyRod, RoditelniyPad, MnozhCh, true},
		testCase{"красным", "красного", ZhenskyRod, DatelniyPad, MnozhCh, true},
		testCase{"красные", "красного", ZhenskyRod, VinitelniyPad, MnozhCh, true},
		testCase{"красными", "красного", ZhenskyRod, TvoritelniyPad, MnozhCh, true},
		testCase{"красных", "красного", ZhenskyRod, PredlozhniyPad, MnozhCh, true},

		// Sr rod
		testCase{"красное", "красное", SredniyRod, ImenitelniyPad, EdinCh, true},
		testCase{"красного", "красное", SredniyRod, RoditelniyPad, EdinCh, true},
		testCase{"красному", "красное", SredniyRod, DatelniyPad, EdinCh, true},
		testCase{"красное", "красное", SredniyRod, VinitelniyPad, EdinCh, true},
		testCase{"красным", "красное", SredniyRod, TvoritelniyPad, EdinCh, true},
		testCase{"красном", "красное", SredniyRod, PredlozhniyPad, EdinCh, true},

		testCase{"красные", "красные", SredniyRod, ImenitelniyPad, MnozhCh, true},
		testCase{"красных", "красные", SredniyRod, RoditelniyPad, MnozhCh, true},
		testCase{"красным", "красные", SredniyRod, DatelniyPad, MnozhCh, true},
		testCase{"красные", "красные", SredniyRod, VinitelniyPad, MnozhCh, true},
		testCase{"красными", "красные", SredniyRod, TvoritelniyPad, MnozhCh, true},
		testCase{"красных", "красные", SredniyRod, PredlozhniyPad, MnozhCh, true},
	}

	for _, testCase := range cases {

		res := ""
		var err error

		if testCase.isAdj {
			res, err = dic.AdjectiveInTypedForm(testCase.in, testCase.wType,
				testCase.wForm, testCase.chisl)
		} else {
			res, err = dic.NounInTypedForm(testCase.in, testCase.wType,
				testCase.wForm, testCase.chisl)
		}

		if err != nil {
			t.Error(err)
		}
		if res != testCase.out {
			t.Errorf("Actual: %q != Expected: %q", res, testCase.out)
			t.Log(dic.WordVariants(testCase.in))
		}
	}

	s1, s2, _ := dic.WordsToSameForm("Цвет", "Красный", true, RoditelniyPad, EdinCh)
	if s1 != "цвета" || s2 != "красного" {
		t.Errorf("Wrong: %s %s", s1, s2)
	}
	sp1, sp2, _ := dic.WordsToSameForm("Цвет", "Красный", true, RoditelniyPad, MnozhCh)
	if sp1 != "цветов" || sp2 != "красных" {
		t.Errorf("Wrong: %s %s", sp1, sp2)
	}

	s3, s4, _ := dic.WordsToSameForm("Материал", "Стул", true, RoditelniyPad, MnozhCh)
	if s3 != "материалов" || s4 != "стул" {
		t.Errorf("Wrong: %s %s", s3, s4)
	}

	s3, s4, _ = dic.WordsToSameForm("Материал", "кожа", true, RoditelniyPad, MnozhCh)
	if s3 != "материалов" || s4 != "кожа" {
		t.Errorf("Wrong: %s %s", s3, s4)
	}

	wt, err := dic.WordType("Стола")
	if err != nil {
		t.Error(err)
	}
	if wt != MuzhskoyRod {
		t.Error("Not Muzhskoy")
	}

	if dic.TryIsNounInFirstForm("стула") {
		t.Error("Wrong")
	}

	if !dic.TryIsNounInFirstForm("стул") {
		t.Error("Wrong")
	}

	plur, pad, wt, adj, err := dic.WordProp("Материалами")
	if plur != true || pad != TvoritelniyPad || wt != MuzhskoyRod || adj != false || err != nil {
		t.Error("Wrong")
		t.Log(dic.WordProp("Материалами"))
	}

	plur, pad, wt, adj, err = dic.WordProp("Красными")
	if plur != true || pad != TvoritelniyPad || wt != Prilagatelnoe || adj != true || err != nil {
		t.Error("Wrong")
		t.Log(dic.WordProp("Красными"))
	}

	t.Log(dic.TryNounInForm("галстук", RoditelniyPad, MnozhCh))
	t.Log(dic.TryNounInForm("куртка", RoditelniyPad, MnozhCh))
	t.Log(dic.TryNounInForm("штаны", RoditelniyPad, MnozhCh))

	//		t.Log(dic.TryGetNounInForm("бренд", ImenitelniyPad, MnozhCh))

	//	t.Log(dic.TryWordsToSameForm("штаны", "прекрасный", ImenitelniyPad, EdinCh))
	//	t.Log(dic.TryWordsToSameForm("платье", "прекрасный", ImenitelniyPad, MnozhCh))

	//	t.Log(dic.WordVariants("голубой"))
	//	t.Log(dic.WordVariants("серо-голубой"))
	//	t.Log(dic.WordVariants("величественный"))
	//	t.Log(dic.WordVariants("удобный"))
	//	t.Log(dic.WordVariants("легкий"))
	//	t.Log(dic.WordVariants("компактный"))

	//	t.Log(dic.WordVariants("петров"))
	//	t.Log(dic.WordVariants("резка"))
	//	t.Log(dic.WordVariants("бежать"))
	//	t.Log(dic.WordVariants("машина"))
	//	t.Log(dic.WordVariants("машиной"))
	//	t.Log(dic.WordVariants("женщина"))
	//	t.Log(dic.WordVariants("кухня"))
	//	t.Log(dic.WordVariants("трава"))
	//	t.Log(dic.WordVariants("селедка"))
	//
	//	t.Log(dic.WordVariants("бежать"))
	//
	//	t.Log(dic.WordVariants("хлопья"))
	//
	//	//	t.Log(dic.GetWordVariants("белый"))
	//	//	t.Log(dic.GetWordVariants("светлый"))
	//	//	t.Log(dic.GetWordVariants("красивые"))
	//	//	t.Log(dic.GetWordVariants("иванов"))
	//
	//	t.Log(dic.WordVariants("три"))
	//	t.Log(dic.WordVariants("пять"))
	//	t.Log(dic.WordVariants("семнадцать"))
	//	//	t.Log(dic.GetWordVariants("третий"))
	//	t.Log(dic.WordVariants("солнце"))
	//
	//	t.Log(dic.WordVariants("стол"))
	//	t.Log(dic.WordVariants("мужчина"))
	//	t.Log(dic.WordVariants("вариант"))
	//	t.Log(dic.WordVariants("партнер"))
	//	t.Log(dic.WordVariants("карандаш"))
	//	t.Log(dic.WordVariants("напильник"))
	//	t.Log(dic.WordVariants("цвет"))
	//	t.Log(dic.WordVariants("размер"))

}
